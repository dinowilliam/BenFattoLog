﻿namespace BenFattoLog.Application.DTO {
    public class MessageDTO {

        public int Code { get; set; }
        public string Message { get; set;  }
        public string Description { get; set;  }

    }
}